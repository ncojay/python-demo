This is a small demonstration application created using python3, django, and django_rest_framework.
It also includes a library for managing Json Web Tokens.

Set up config:

* Adjust db config settings in /settings.py.  For sqlite3, change the sqlite setting to default.
If using mysql, ensure the mysql driver is loaded and that the db, user, and pw are updated.

* Ensure all dependencies are installed listed in /settings.py for installed apps
`pip3 install [package]`

* From the root directory, run migrations
`python3 mananage.py migrate`

* Start the server
`python3 manage.py runserver`